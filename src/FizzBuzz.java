
public class FizzBuzz {

	public String getFizzBuzz(int number) {
		// 6. The numbers must be returned as a string.
		// THIS IS DONE ALREADY!
		// NO NEED TO DO ANYTHING!
		
		if (number < 0) {
			return "error!";
		}
		else if (number % 15 == 0) {
			return "fizzbuzz";
		}
		else if (number % 5 == 0) {
			return "buzz";
		}
		else if (number % 3 == 0) {
			return "fizz";
		}
		else {
			return String.valueOf(number);
		}
	}
	

}
