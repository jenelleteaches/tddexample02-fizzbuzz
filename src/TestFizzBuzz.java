import static org.junit.Assert.*;

import org.junit.Test;

public class TestFizzBuzz {
	/*
	Create a function that returns: “fizz”, “buzz” or “fizzbuzz”.
	1.  Your function should accept a number > 0.
		---> what should happen if number < 0? -- print "error"
	The function should:
	
	2. Return “fizz” if the number is divisible by 3
	3. Return “buzz” if the number is divisible by 5
	4. Return “fizzbuzz” if the number is divisible by 15
	5. Return the number if no other requirement is fulfilled. 
	6. The numbers must be returned as a string.
	
	Stub: getFizzBuzz(number)
	*/
	
	@Test
	public void testInputGreaterThanZero() {
		FizzBuzz fb = new FizzBuzz();
		String result = fb.getFizzBuzz(1);
		assertEquals("1", result);
	}
		
	@Test
	public void testInputLessThanZero() {
		FizzBuzz fb = new FizzBuzz();
		String result = fb.getFizzBuzz(-5);
		assertEquals("error!", result);
	}
	
	// -----------------------------------------
	// Test cases for Requirement 2
	// -----------------------------------------
	@Test
	public void testInputIsDivisibleBy3() {
		// REQ2 - GREEN - updated test case with an input
		// that will make the test case pass
		FizzBuzz fb = new FizzBuzz();
		String result = fb.getFizzBuzz(18);
		assertEquals("fizz", result);
	}
	
	// -----------------------------------------
	// Test cases for Requirement 3
	// -----------------------------------------
	@Test 
	public void testInputIsDivisibleBy5() {
		FizzBuzz fb = new FizzBuzz();
		String result = fb.getFizzBuzz(5);
		assertEquals("buzz", result);
	}
	
	// -----------------------------------------
	// Test cases for Requirement 4
	// -----------------------------------------
	@Test 
	public void testInputIsDivisibleBy15() {
		FizzBuzz fb = new FizzBuzz();
		String result = fb.getFizzBuzz(90);
		assertEquals("fizzbuzz", result);
	}
	
	
	// -----------------------------------------
	// Test cases for Requirement 5
	// -----------------------------------------
	@Test 
	public void testInputIsSomethingElse() {
		FizzBuzz fb = new FizzBuzz();
		String result = fb.getFizzBuzz(7);
		assertEquals("7", result);
	}
	
	

	
}
