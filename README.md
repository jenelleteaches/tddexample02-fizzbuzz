# FizzBuzz using TDD

This is ONE possible solution for Fizzbuzz using TDD (Test Driven Development).

## How to Use this Repository

* TDD is about the PROCESS
* Look at the commit history of this repo to see where the "red", "green", "refactors" are.

## What is Fizzbuzz?

Create a function that returns: "fizz", "buzz" or "fizzbuzz".

Your function should accept a number > 0.

The function should:

* Return "fizz" if the number is divisible by 3
* Return "buzz" if the number is divisible by 5
* Return "fizzbuzz" if the number is divisible by 15
* Return the number if no other requirement is fulfilled. The numbers must be returned as a string.

